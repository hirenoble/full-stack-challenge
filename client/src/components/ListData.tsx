import Box from '@mui/material/Box';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { CircularProgress } from '@mui/material';
import { People, Planet } from '../types';
import Typography from '@mui/material/Typography';
import { useState } from 'react';
interface ListDataProps {
  data: People[] | Planet[];
  columns: GridColDef[];
}
const ListData: React.FC<ListDataProps> = ({ data, columns }) => {
  const [pageSize, setPageSize] = useState<number>(10);
  return data.length ? (
    <Box sx={{ width: '100%' }}>
      <DataGrid
        autoHeight
        getRowId={(row) => row.created}
        rows={data}
        pageSize={pageSize}
        onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
        rowsPerPageOptions={[10, 25, 50]}
        pagination
        columns={columns}
        experimentalFeatures={{ newEditingApi: true }}
      />
    </Box>
  ) : (
    <>
      <Box sx={{ width: '100%', mx: 'auto', m: 1 }}>
        <Typography variant="h6">Loading...</Typography>
        <CircularProgress disableShrink />
      </Box>
    </>
  );
};

export default ListData;
