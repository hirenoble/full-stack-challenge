import React, { useState, useEffect } from 'react';
import Container from '@mui/material/Container';
import { People, Planet } from './types';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import ListData from './components/ListData';
import {
  fetchPeople,
  fetchPlanets,
  peopleColumns,
  planetsColumns,
} from './helper';

export default function App() {
  const [people, setPeople] = useState<People[]>([]);
  const [planets, setPlanets] = useState<Planet[]>([]);

  const [type, setType] = React.useState('');

  const handleChange = (event: SelectChangeEvent) => {
    setType(event.target.value);
  };

  useEffect(() => {
    const getPeople = async () => {
      const peopleList = await fetchPeople();
      setPeople(peopleList);
    };
    const getPlanets = async () => {
      const planetsList = await fetchPlanets();
      setPlanets(planetsList);
    };
    getPeople();
    getPlanets();
  });
  return (
    <Container sx={{ p: 5 }}>
      <FormControl variant="standard" sx={{ p: 1, minWidth: 200 }}>
        <InputLabel id="demo-simple-select-standard-label">
          Select a Data Type
        </InputLabel>
        <Select
          labelId="demo-simple-select-standard-label"
          id="demo-simple-select-standard"
          value={type}
          onChange={handleChange}
          label="Select Data Type"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value="people">People</MenuItem>
          <MenuItem value="planets">Planets</MenuItem>
        </Select>
      </FormControl>
      {type === 'people' && <ListData data={people} columns={peopleColumns} />}
      {type === 'planets' && (
        <ListData data={planets} columns={planetsColumns} />
      )}
    </Container>
  );
}
