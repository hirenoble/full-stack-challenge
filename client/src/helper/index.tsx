import config from '../config.json';
import { GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
export const fetchPeople = async () => {
  const res = await fetch(config.API_URL + '/people');
  return await res.json();
};
export const fetchPlanets = async () => {
  const res = await fetch(config.API_URL + '/planets');
  return await res.json();
};

export const peopleColumns: GridColDef[] = [
  { field: 'name', headerName: 'Name', width: 200 },
  {
    field: 'films',
    headerName: 'No. of Films',
    width: 100,
    valueGetter: (params: GridValueGetterParams) => {
      return `${params.row.films.length}`;
    },
  },
  {
    field: 'hair_color',
    headerName: 'Hair Color',
    width: 150,
  },
  {
    field: 'eye_color',
    headerName: 'Eye Color',
    width: 150,
  },
  {
    field: 'birth_year',
    headerName: 'Birth Year',
    width: 150,
  },
  {
    field: 'height',
    headerName: 'Height',
    width: 150,
    sortComparator: (a, b) => {
      let numA = Number(a);
      let numB = Number(b);

      if (isNaN(numA)) {
        return -1;
      }
      if (isNaN(numB)) {
        return 1;
      }
      if (numA > numB) {
        return 1;
      } else if (numA < numB) {
        return -1;
      } else {
        return 0;
      }
    },
  },
  {
    field: 'mass',
    headerName: 'Mass',
    width: 150,
    sortComparator: (a, b) => {
      let numA = Number(a);
      let numB = Number(b);

      if (isNaN(numA)) {
        return -1;
      }
      if (isNaN(numB)) {
        return 1;
      }
      if (numA > numB) {
        return 1;
      } else if (numA < numB) {
        return -1;
      } else {
        return 0;
      }
    },
  },
];

export const planetsColumns: GridColDef[] = [
  { field: 'name', headerName: 'Name', width: 100 },
  { field: 'diameter', headerName: 'Diameter', width: 100 },
  { field: 'climate', headerName: 'Climate', width: 100 },
  { field: 'terrain', headerName: 'Terrain', width: 250 },
  { field: 'population', headerName: 'Population', width: 150 },
  { field: 'residents', headerName: 'Residents', width: 400 },
];
