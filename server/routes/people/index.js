const express = require('express');
const { getAllPeople } = require('../../helpers/peopleHelper');
const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    let results = await getAllPeople(req.query.sortBy);
    res.send(results);
  } catch (e) {
    res.status(500).send({
      error: 'something went wrong',
    });
  }
});

module.exports = router;
