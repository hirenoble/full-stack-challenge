const express = require('express');
const { getAllPlanets } = require('../../helpers/planetsHelper');
const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    let results = await getAllPlanets();
    res.send(results);
  } catch (e) {
    res.status(500).send({
      error: 'something went wrong',
    });
  }
});

module.exports = router;
