const axios = require('axios');
const makeRequest = async (options) => {
  return axios(options)
    .then(function (response) {
      let data = response.data;
      return data;
    })
    .catch(function (error) {
      throw error;
    });
};

module.exports = makeRequest;
