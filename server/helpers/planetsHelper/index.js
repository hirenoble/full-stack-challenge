const config = require('../../config.json');
const makeRequest = require('../makeRequest');
const { getResidentName } = require('../peopleHelper');

const getAllPlanets = async () => {
  try {
    let response = [];
    const options = {
      method: 'get',
      url: `${config.SOURCE_API_URL}/planets`,
    };

    let { count, next, results } = await makeRequest(options);

    if (!count) {
      return [];
    }

    results = await Promise.all(
      results.map(async (result) => {
        result.residents = await Promise.all(
          result.residents.map(
            async (resident) => await getResidentName(resident)
          )
        );
        return result;
      })
    );

    response.push(...results);

    if (next) {
      let limit = Math.ceil(count / results.length);
      let endpoints = [...Array(limit - 1).keys()].map(
        (x) => options.url + `?page=${x + 2}`
      );

      await Promise.all(
        endpoints.map(async (endpoint) => {
          let { results = [] } = await makeRequest({
            ...options,
            url: endpoint,
          });
          results = await Promise.all(
            results.map(async (result) => {
              result.residents = await Promise.all(
                result.residents.map(
                  async (resident) => await getResidentName(resident)
                )
              );
              return result;
            })
          );
          response.push(...results);
        })
      );
    }
    return response;
  } catch (e) {
    throw e;
  }
};

module.exports = { getAllPlanets };
