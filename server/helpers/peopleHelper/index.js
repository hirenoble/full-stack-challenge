const config = require('../../config.json');
const makeRequest = require('../makeRequest');

const getAllPeople = async (sortBy) => {
  try {
    let response = [];
    const options = {
      method: 'get',
      url: `${config.SOURCE_API_URL}/people`,
    };
    let { count, next, results } = await makeRequest(options);

    if (!count) {
      return [];
    }

    response.push(...results);

    if (next) {
      let limit = Math.ceil(count / results.length);
      let endpoints = [...Array(limit - 1).keys()].map(
        (x) => options.url + `?page=${x + 2}`
      );

      await Promise.all(
        endpoints.map(async (endpoint) => {
          let { results = [] } = await makeRequest({
            ...options,
            url: endpoint,
          });
          response.push(...results);
        })
      );
    }

    if (sortBy) {
      response.sort((a, b) => sortingLogic(a, b, sortBy));
    }
    return response;
  } catch (e) {
    throw e;
  }
};

const getResidentName = async (url) => {
  const options = {
    method: 'get',
    url,
  };
  let { name } = await makeRequest(options);
  return name;
};

const sortingLogic = (a, b, sortBy) => {
  // always return 'unknown' values at front
  if (a[sortBy] === 'unknown') {
    return -1;
  }
  if (b[sortBy] === 'unknown') {
    return 1;
  }

  let numA = Math.abs(a[sortBy]);
  let numB = Math.abs(b[sortBy]);

  if (isNaN(numA) && isNaN(numB)) {
    if (a[sortBy] > b[sortBy]) {
      return 1;
    } else if (a[sortBy] < b[sortBy]) {
      return -1;
    } else {
      return 0;
    }
  } else {
    if (numA > numB) {
      return 1;
    } else if (numA < numB) {
      return -1;
    } else {
      return 0;
    }
  }
};

module.exports = { getAllPeople, getResidentName };
